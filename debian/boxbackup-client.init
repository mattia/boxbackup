#! /bin/sh
#
### BEGIN INIT INFO
# Provides: boxbackup-client
# Required-Start:  $syslog $remote_fs $network
# Required-Stop: $syslog $remote_fs $network
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: boxbackup client
# Description: Init script to start and stop the boxbackup client
### END INIT INFO

. /lib/lsb/init-functions

PATH=/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/sbin/bbackupd
NAME=bbackupd
DESC=boxbackup-client
CONF=/etc/boxbackup/bbackupd.conf

test -f $DAEMON || exit 0

test -f $CONF || exit 0

PIDFILE=`grep 'PidFile' $CONF | sed 's/[[:space:]]*PidFile[[:space:]]*=[[:space:]]*\(\/[A-Za-z0-9/]*\)/\1/'`
CERTFILE=`grep 'CertificateFile' $CONF | sed 's/[[:space:]]*CertificateFile[[:space:]]*=[[:space:]]*\(\/[A-Za-z0-9/]*\)/\1/'`
ACCNUM=`grep 'AccountNumber' $CONF | sed 's/[[:space:]]*AccountNumber[[:space:]]*=[[:space:]]*\([A-Za-z0-9/]*\)/\1/'`

[ -z $PIDFILE ] && PIDFILE="/var/run/bbackupd.pid"

# Don't start if certificate file or account number are not present
[ ! -e $CERTFILE -o -z $ACCNUM ] && exit 0

set -e

case "$1" in
  start)
	echo -n "Starting $DESC: "
	start-stop-daemon --start --quiet --pidfile $PIDFILE \
		--exec $DAEMON -- $CONF
	echo "$NAME."
	;;
  stop)
	echo -n "Stopping $DESC: "
	start-stop-daemon --oknodo --retry 5 --stop --quiet --pidfile $PIDFILE \
		--exec $DAEMON
	echo "$NAME."
	;;
  reload|force-reload)
	echo "Reloading $DESC configuration files."
	start-stop-daemon --stop --signal 1 --quiet --pidfile \
		 $PIDFILE --exec $DAEMON -- $CONF
	;;
  restart)
	echo -n "Restarting $DESC: "
	start-stop-daemon --oknodo --retry 5 --stop --quiet --pidfile \
		$PIDFILE --exec $DAEMON
	sleep 1
	start-stop-daemon --start --quiet --pidfile \
		$PIDFILE --exec $DAEMON -- $CONF
	echo "$NAME."
	;;
  *)
	N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop|restart|reload|force-reload}" >&2
	#echo "Usage: $N {start|stop|restart|force-reload}" >&2
	exit 1
	;;
esac

exit 0
